package org.bibsonomy.dblp;

import java.io.File;
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

import org.bibsonomy.common.enums.PostUpdateOperation;
import org.bibsonomy.dblp.util.DBLPUtils;
import org.bibsonomy.model.BibTex;
import org.bibsonomy.model.PersonName;
import org.bibsonomy.model.Post;
import org.bibsonomy.model.Resource;
import org.bibsonomy.model.logic.LogicInterface;
import org.bibsonomy.model.util.PersonNameParser.PersonListParserException;
import org.bibsonomy.model.util.PersonNameUtils;
import org.bibsonomy.rest.client.RestLogicFactory;

/**
 * removes person id identifier
 * 
 * @author dzo
 */
public class DBLPPersonNameFix {
	private static final int LIMIT = 100 * 1000;
	/**
	 * @param args
	 * 				args[0] username
	 * 				args[1] apikey
	 * 				args[2] host
	 * @throws Exception 
	 * @throws  
	 */
	public static void main(String[] args) throws Exception {
		final RestLogicFactory factory = new RestLogicFactory(args.length == 4 ? args[3] : RestLogicFactory.BIBSONOMY_API_URL);
		final String apiUsername = args[1];
		final LogicInterface logic = factory.getLogicAccess(apiUsername, args[2]);
		int totalCount = 0;
		
		final Properties properties = new Properties();
		final FileInputStream stream = new FileInputStream(new File(args[0]));
		properties.load(stream);
		stream.close();
		
		final String databaseUrl = properties.getProperty("database.main.url");
		final String username = properties.getProperty("database.main.username");
		final String password = properties.getProperty("database.main.password");
		
		
		Class.forName("com.mysql.jdbc.Driver");
		
		final Connection connection = DriverManager.getConnection(databaseUrl, username, password);
		int count = 0;
		int offset = 0;
		do {
			final PreparedStatement prepareStatement = connection.prepareStatement("SELECT simhash2, author, editor FROM bibtex WHERE user_name = ? LIMIT " + LIMIT + " OFFSET ?");
			prepareStatement.setString(1, apiUsername);
			prepareStatement.setInt(2, offset);
			final ResultSet result = prepareStatement.executeQuery();
			count = 0;
			while (result.next()) {
				count++;
				final String intrahash = result.getString("simhash2");
				final String author = result.getString("author");
				final String editor = result.getString("editor");
				boolean authorsChanged = false;
				boolean editorsChanged = false;
				List<PersonName> newAuthors = null;
				try {
					List<PersonName> authors = PersonNameUtils.discoverPersonNames(author);
					newAuthors = normPersons(authors);
					authorsChanged = !authors.equals(newAuthors);
				} catch (PersonListParserException e) {
					System.out.println("author skip " + intrahash);
				}
				List<PersonName> newEditors = null;
				try {
					List<PersonName> editors = PersonNameUtils.discoverPersonNames(editor);
					newEditors = normPersons(editors);
					editorsChanged = !editors.equals(newEditors);
				} catch (PersonListParserException e) {
					System.out.println("editor skip " + intrahash);
				}
				
				final boolean changed = authorsChanged || editorsChanged;
				if (changed) {
					System.out.println("updating post " + intrahash);
					@SuppressWarnings("unchecked") // ok only publications queried
					final Post<BibTex> post = (Post<BibTex>) logic.getPostDetails(intrahash, apiUsername);
					final BibTex publiation = post.getResource();
					if (authorsChanged && newAuthors != null) {
						publiation.setAuthor(newAuthors);
					}
					
					if (editorsChanged && newEditors != null) {
						publiation.setEditor(newEditors);
					}
					
					try {
						logic.updatePosts(Collections.<Post<? extends Resource>>singletonList(post), PostUpdateOperation.UPDATE_ALL);
					} catch (Exception e) {
						System.err.println("failed to update " + post.getResource().getInterHash());
						e.printStackTrace();
					}
				}
			}
			result.close();
			prepareStatement.close();
			totalCount += Math.min(LIMIT, count);
			System.out.println("processed " + totalCount + " posts");
			offset += LIMIT;
		} while (count == LIMIT);
		connection.close();
		System.out.println("done!");
	}

	/**
	 * @param author
	 * @return
	 */
	private static List<PersonName> normPersons(final List<PersonName> author) {
		final List<PersonName> newAuthors = new LinkedList<PersonName>();
		for (final PersonName personName : author) {
			final String serializePersonName = PersonNameUtils.serializePersonName(personName, false);
			final String normed = DBLPUtils.normPersonName(serializePersonName);
			if (!normed.equals(serializePersonName)) {
				newAuthors.add(PersonNameUtils.discoverPersonNamesIgnoreExceptions(normed).get(0));
			} else {
				newAuthors.add(personName);
			}
		}
		return newAuthors;
	}
}
