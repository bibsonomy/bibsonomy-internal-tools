package org.bibsonomy.search.es.management;

import java.util.Arrays;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import org.bibsonomy.model.Resource;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * offline index updater
 *
 * @author dzo
 */
public class ElasticsearchIndexUpdater {
	
	private static final class IndexUpdaterTask extends TimerTask {
		private final ElasticsearchManager<? extends Resource> manager;
		private final String resourceName;
		private final boolean updateBoth;
		private final String[] indices;

		/**
		 * @param manager
		 * @param resourceName
		 * @param updateBoth
		 * @param indices
		 */
		public IndexUpdaterTask(ElasticsearchManager<? extends Resource> manager, String resourceName, boolean updateBoth, String[] indices) {
			super();
			this.manager = manager;
			this.resourceName = resourceName;
			this.updateBoth = updateBoth;
			this.indices = indices;
		}

		/* (non-Javadoc)
		 * @see java.util.TimerTask#run()
		 */
		@Override
		public void run() {
			final Date date = new Date();
			System.out.println("updating " + this.resourceName + " index (" + date + ").");
			
			if (this.indices != null) {
				for (final String index : this.indices) {
					System.out.println("updating " + index);
					this.manager.updateIndex(index);
				}
			} else {
				this.manager.updateIndex(); // update first index
				if (this.updateBoth) {
					System.out.println("updated " + this.resourceName + " index.");
					System.out.println("updating second " + this.resourceName + " index (" + date + ").");
					this.manager.updateIndex(); // update second index
				}
			}
			
			System.out.println("updated " + this.resourceName + " index.");
		}
	}
	
	/**
	 * specify the resource class and the period (optional if both indices should
	 * be updated) [list of indices to update]
	 * @param args
	 */
	public static void main(String[] args) {
		if (args.length == 0) {
			System.exit(-1);
		}
		
		final String resourceClasses = args[0];
		final long period = Long.parseLong(args[1]);
		final boolean updateBoth = args.length > 2 ? Boolean.parseBoolean(args[2]) : false;
		
		final String[] indices;
		
		if (args.length > 3) {
			indices = Arrays.copyOfRange(args, 3, args.length - 1);
		} else {
			indices = null;
		}
		
		final String[] resourceClassesToUpdate;
		
		if ("all".equals(resourceClasses)) {
			if (indices != null) {
				throw new IllegalArgumentException("when specified index names all as resource class is not supported");
			}
			resourceClassesToUpdate = new String[] { "Bookmark", "Publication" };
		} else {
			resourceClassesToUpdate = new String[] { resourceClasses };
		}
		
		final ClassPathXmlApplicationContext beanFactory = new ClassPathXmlApplicationContext(ElasticsearchIndexGenerator.CONFIG_LOCATION);
		
		final Timer timer = new Timer();
		long periodOffset = period / resourceClassesToUpdate.length;
		int count = 0;
		for (String resourceClass : resourceClassesToUpdate) {
			@SuppressWarnings("unchecked") // ok
			final ElasticsearchManager<? extends Resource> manager = (ElasticsearchManager<? extends Resource>) beanFactory.getBean("elasticsearch" + resourceClass + "Manager");
			
			final IndexUpdaterTask task = new IndexUpdaterTask(manager, resourceClass, updateBoth, indices);
			timer.scheduleAtFixedRate(task, count * periodOffset, period);
			count++;
		}
	}
}
