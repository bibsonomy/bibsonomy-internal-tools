package org.bibsonomy.search.es.management;

import org.bibsonomy.model.Resource;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author jil
 * @author dzo
 */
public class ElasticsearchIndexGenerator {
	/** the es context for offline use */
	public static final String CONFIG_LOCATION = "classpath:ElasticSearchGeneratorContext.xml";
	
	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main(final String[] args) throws Exception {
		if (args.length == 0) {
			System.out.println("usage: " + ElasticsearchIndexGenerator.class.getName() + " resourceClassName generateBothIndices activate");
			System.exit(-1);
		}

		final String resourceClass = args[0];
		final boolean bothIndices = Boolean.parseBoolean(args[1]);
		final boolean activateIndex = Boolean.parseBoolean(args[2]);
		
		final ClassPathXmlApplicationContext beanFactory = new ClassPathXmlApplicationContext(CONFIG_LOCATION);
		
		@SuppressWarnings("unchecked") // ok
		final ElasticsearchManager<? extends Resource> manager = (ElasticsearchManager<? extends Resource>) beanFactory.getBean("elasticsearch" + resourceClass + "Manager");
		
		System.out.println("Started generating index.");
		manager.generateIndex(false, activateIndex);
		System.out.println("Finished generating index.");
		
		if (bothIndices) {
			System.out.println("Started generating second index.");
			manager.generateIndex(false, activateIndex);
			System.out.println("Finished generating second index.");
		}
		
		beanFactory.close();
	}
}
