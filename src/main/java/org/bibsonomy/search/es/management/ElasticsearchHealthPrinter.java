package org.bibsonomy.search.es.management;

import java.util.List;

import org.bibsonomy.model.Resource;
import org.bibsonomy.search.model.SearchIndexInfo;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * prints the search index infos to standard out
 *
 * @author dzo
 */
public class ElasticsearchHealthPrinter {
	/**
	 * place specify the resource type
	 * @param args
	 */
	public static void main(String[] args) {
		if (args.length == 0) {
			System.exit(-1);
		}
		
		final String resourceClass = args[0];
		
		final ClassPathXmlApplicationContext beanFactory = new ClassPathXmlApplicationContext(ElasticsearchIndexGenerator.CONFIG_LOCATION);
		@SuppressWarnings("unchecked") // ok, configuration via xml
		final ElasticsearchManager<? extends Resource> manager = (ElasticsearchManager<? extends Resource>) beanFactory.getBean("elasticsearch" + resourceClass + "Manager");
		
		final List<SearchIndexInfo> infos = manager.getIndexInformations();
		
		for (final SearchIndexInfo searchIndexInfo : infos) {
			System.out.println(searchIndexInfo.getId());
			System.out.println(searchIndexInfo.getState());
			System.out.println(searchIndexInfo.getSyncState().getLast_tas_id());
			System.out.println(searchIndexInfo.getSyncState().getLast_log_date());
			System.out.println("====");
		}
	}
}
