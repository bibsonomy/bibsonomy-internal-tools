package org.bibsonomy.search.es.tool;

import java.util.Collections;
import java.util.List;

import org.bibsonomy.common.enums.SearchType;
import org.bibsonomy.model.Post;
import org.bibsonomy.model.Resource;
import org.bibsonomy.model.enums.Order;
import org.bibsonomy.search.es.management.ElasticsearchIndexGenerator;
import org.bibsonomy.services.searcher.ResourceSearch;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * tests the connection to the es cluster
 * @author dzo
 */
public class ConnectionTester {
	/**
	 * @param args [0] the resource class to test
	 */
	@SuppressWarnings("unchecked")
	public static void main(String[] args) {
		final ClassPathXmlApplicationContext beanFactory = new ClassPathXmlApplicationContext(ElasticsearchIndexGenerator.CONFIG_LOCATION);
		final ResourceSearch<? extends Resource> search = (ResourceSearch<? extends Resource>) beanFactory.getBean("elasticsearch" + args[0] + "Search");
		final List<? extends Post<? extends Resource>> posts = search.getPosts("", "", null, null, Collections.emptyList(), SearchType.LOCAL, "recommender", "", "", "", null, "", "", "", null, Order.ADDED, 10, 0);
		for (Object object : posts) {
			System.out.println(object);
		}
	}
}
