package org.bibsonomy.search;

import java.io.File;
import java.util.Collection;

import org.bibsonomy.common.enums.LayoutPart;
import org.bibsonomy.common.enums.PreviewSize;
import org.bibsonomy.model.Document;
import org.bibsonomy.model.util.file.UploadedFile;
import org.bibsonomy.services.filesystem.FileLogic;
import org.bibsonomy.services.filesystem.extension.ExtensionChecker;
import org.bibsonomy.util.file.FileUtil;

/**
 * TODO: use ServerFileLogic
 * @author dzo
 */
public class FileLogicImpl implements FileLogic {

	private final String path;

	/**
	 * @param path
	 */
	public FileLogicImpl(final String path) {
		this.path = path;
	}

	/* (non-Javadoc)
	 * @see org.bibsonomy.services.filesystem.ProfilePictureLogic#saveProfilePictureForUser(java.lang.String, org.bibsonomy.model.util.file.UploadedFile)
	 */
	@Override
	public void saveProfilePictureForUser(String username, UploadedFile pictureFile) throws Exception {
		throw new UnsupportedOperationException();
	}

	/* (non-Javadoc)
	 * @see org.bibsonomy.services.filesystem.ProfilePictureLogic#deleteProfilePictureForUser(java.lang.String)
	 */
	@Override
	public void deleteProfilePictureForUser(String username) {
		throw new UnsupportedOperationException();
	}

	/* (non-Javadoc)
	 * @see org.bibsonomy.services.filesystem.ProfilePictureLogic#getProfilePictureForUser(java.lang.String)
	 */
	@Override
	public File getProfilePictureForUser(String username) {
		throw new UnsupportedOperationException();
	}

	/* (non-Javadoc)
	 * @see org.bibsonomy.services.filesystem.TempFileLogic#getTempFile(java.lang.String)
	 */
	@Override
	public File getTempFile(String name) {
		throw new UnsupportedOperationException();
	}

	/* (non-Javadoc)
	 * @see org.bibsonomy.services.filesystem.TempFileLogic#writeTempFile(org.bibsonomy.model.util.file.UploadedFile, org.bibsonomy.services.filesystem.extension.ExtensionChecker)
	 */
	@Override
	public File writeTempFile(UploadedFile file, ExtensionChecker extensionChecker) throws Exception {
		throw new UnsupportedOperationException();
	}

	/* (non-Javadoc)
	 * @see org.bibsonomy.services.filesystem.TempFileLogic#deleteTempFile(java.lang.String)
	 */
	@Override
	public void deleteTempFile(String name) {
		throw new UnsupportedOperationException();
	}

	/* (non-Javadoc)
	 * @see org.bibsonomy.services.filesystem.JabRefFileLogic#writeJabRefLayout(java.lang.String, org.bibsonomy.model.util.file.UploadedFile, org.bibsonomy.common.enums.LayoutPart)
	 */
	@Override
	public Document writeJabRefLayout(String username, UploadedFile file, LayoutPart layoutPart) throws Exception {
		throw new UnsupportedOperationException();
	}

	/* (non-Javadoc)
	 * @see org.bibsonomy.services.filesystem.JabRefFileLogic#deleteJabRefLayout(java.lang.String)
	 */
	@Override
	public boolean deleteJabRefLayout(String hash) {
		throw new UnsupportedOperationException();
	}

	/* (non-Javadoc)
	 * @see org.bibsonomy.services.filesystem.JabRefFileLogic#validJabRefLayoutFile(org.bibsonomy.model.util.file.UploadedFile)
	 */
	@Override
	public boolean validJabRefLayoutFile(UploadedFile file) {
		throw new UnsupportedOperationException();
	}

	/* (non-Javadoc)
	 * @see org.bibsonomy.services.filesystem.JabRefFileLogic#allowedJabRefFileExtensions()
	 */
	@Override
	public Collection<String> allowedJabRefFileExtensions() {
		throw new UnsupportedOperationException();
	}

	/* (non-Javadoc)
	 * @see org.bibsonomy.services.filesystem.DocumentFileLogic#getFileForDocument(org.bibsonomy.model.Document)
	 */
	@Override
	public File getFileForDocument(Document document) {
		final File file = new File(this.getFilePath(document.getFileHash()));
		file.setReadOnly();
		return file;
	}
	
	private String getFilePath(String fileHash) {
		return FileUtil.getFilePath(this.path, fileHash);
	}

	@Override
	public File getContentCacheFileForDocument(final Document document) {
		return new File(this.getFilePath(document.getFileHash()) + "_content.cache");
	}

	/* (non-Javadoc)
	 * @see org.bibsonomy.services.filesystem.DocumentFileLogic#getPreviewFile(org.bibsonomy.model.Document, org.bibsonomy.common.enums.PreviewSize)
	 */
	@Override
	public File getPreviewFile(Document document, PreviewSize preview) {
		throw new UnsupportedOperationException();
	}

	/* (non-Javadoc)
	 * @see org.bibsonomy.services.filesystem.DocumentFileLogic#saveDocumentFile(java.lang.String, org.bibsonomy.model.util.file.UploadedFile)
	 */
	@Override
	public Document saveDocumentFile(String name, UploadedFile file) throws Exception {
		throw new UnsupportedOperationException();
	}

	/* (non-Javadoc)
	 * @see org.bibsonomy.services.filesystem.DocumentFileLogic#deleteFileForDocument(java.lang.String)
	 */
	@Override
	public boolean deleteFileForDocument(String fileHash) {
		throw new UnsupportedOperationException();
	}

	/* (non-Javadoc)
	 * @see org.bibsonomy.services.filesystem.DocumentFileLogic#getDocumentExtensionChecker()
	 */
	@Override
	public ExtensionChecker getDocumentExtensionChecker() {
		throw new UnsupportedOperationException();
	}

	/* (non-Javadoc)
	 * @see org.bibsonomy.services.filesystem.CslFileLogic#writeCSLLayout(java.lang.String, org.bibsonomy.model.util.file.UploadedFile)
	 */
	@Override
	public Document writeCSLLayout(String username, UploadedFile file) throws Exception {
		throw new UnsupportedOperationException();
	}

	/* (non-Javadoc)
	 * @see org.bibsonomy.services.filesystem.CslFileLogic#deleteCSLLayout(java.lang.String)
	 */
	@Override
	public boolean deleteCSLLayout(String hash) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean isValidCSLLayoutFile(UploadedFile file) {
		return false;
	}

	/* (non-Javadoc)
	 * @see org.bibsonomy.services.filesystem.CslFileLogic#allowedCSLFileExtensions()
	 */
	@Override
	public Collection<String> allowedCSLFileExtensions() {
		throw new UnsupportedOperationException();
	}

}
