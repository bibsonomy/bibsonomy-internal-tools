package org.bibsonomy.search;

import org.bibsonomy.model.Post;
import org.bibsonomy.model.Resource;
import org.bibsonomy.services.information.InformationService;

/**
 * noop implementation of a {@link InformationService}
 *
 * @author dzo
 */
public class NoopInfoService implements InformationService {

	@Override
	public void createdPost(String username, Post<? extends Resource> post) {
		// noop
	}
}
