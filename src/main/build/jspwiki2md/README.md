JSPWiki2MD
==========

Converts all *.txt files in the given directory from JSPWiki syntax to markdown. For every file `file.txt` a new file `file.md` is created.

Usage: `./jspwiki2md.sh directory`

Requires

* sed
* perl
* [Pandoc ](https://pandoc.org)
