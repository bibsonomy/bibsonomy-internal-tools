#!/bin/bash

function convert {

	if [ ! -f $1 ]; then
		echo "Error: $1 is not a valid file"
		exit 1
	fi
	
	base=$(basename $1 ".txt")
	dir=$(dirname $1)
	sed "s/\[{\\s*Image[^}]*src='\([^']*\)'[^}]*}\]/\[File:\1\]/g" $1 | sed "s/\[{SET lang_\([^=]*\)='\([^']*\)'}\]/<-- \1: \2 --\>/g" | sed "s/\[{\$jspwiki\.projname}\]/\${project.name}/g" | sed "s/\[\\s*{\\s*SET alias='\([^']*\)'}\]/<-- redirect:\1-->/g" | perl JSP2MediaWiki.pl - | pandoc -t markdown -f mediawiki - | sed "s/\\\\\${/\${/g" | sed "s/^\&lt;--\(.*\)--\&gt;/<\!--\1-->/g" | sed "s/\\\\$/  /g" | sed "s/Bib:\//\${project.home}/g" > ${dir}/${base}.md
}

for file in $1/*.txt; do
  echo "converting $file"
	convert $file
done
