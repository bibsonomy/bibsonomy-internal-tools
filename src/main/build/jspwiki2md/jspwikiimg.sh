#!/bin/bash

function copyFile {
  name=`basename $1 "-att" | tr '[:upper:]' '[:lower:]'`
  mkdir -p img/$name
  
  for imageFolder in $1/*-dir; do
    imageName=`basename $imageFolder "-dir"`
    echo " copying image $imageName"
    imageFile=`ls $imageFolder | sort | tail -n 2 | head -n 1`
    cp $imageFolder/$imageFile img/$name/$imageName
  done
}

mkdir -p img

for file in $1/*-att; do
  echo "getting images ($file)"
  copyFile $file
done
